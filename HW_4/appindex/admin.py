from django.contrib import admin

from .models import Services, Request, Price_list


admin.site.register(Services)
admin.site.register(Request)
admin.site.register(Price_list)
# Register your models here.
