def transfrom(all_services):
    for service, i in zip(all_services, range(1, len(all_services)+1)):
        service.id = '{:0>2}'.format(str(i))
        service.price = (' '.join([str(service.price)[::-1][i:i+3] for i in range(0,len(str(service.price)),3)]))[::-1]
    return all_services