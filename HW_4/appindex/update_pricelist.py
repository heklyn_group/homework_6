from .models import Services, Price_list
import random
def update_list():
    all_services = Services.objects.all()
    for service in all_services:
        if not(Price_list.objects.filter(name=service.name, size='Маленький').exists()):
            elem = Price_list(name=service.name, size='size_1', price = random.randint(1 , 10) * 10000,
                              date = random.randint(1, 4))
            elem.save()
        if not(Price_list.objects.filter(name=service.name, size='Большой').exists()):
            elem = Price_list(name=service.name, size='size_2', price = random.randint(1, 10) * 100000,
                              date = random.randint(3, 6))
            elem.save()