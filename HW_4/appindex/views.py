from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Request, Services, Price_list
from .servies_transform import transfrom
from .get_dict import get
from .update_pricelist import update_list

def index(request):
    all_services = Services.objects.all()[:]

    all_services = transfrom(all_services)
    services_one, services_two, classes = get(all_services)
    update_list()
    calc = {'one' : '', 'two' : 0, 'three' : 0}
    return render(request, 'index.html', {'all_services' : all_services, 'services_one' : services_one,
                                          'services_two' : services_two, 'classes' : classes,
                                          'calc': calc})




def create(request):
    if request.method == "POST":
        req = Request()
        req.name = request.POST.get("name")
        req.email = request.POST.get("email")
        req.select = request.POST.get("select")
        req.commen = request.POST.get("commen")
        req.save()
    return HttpResponseRedirect("/")

def calc(request):
    price = 0
    date = 0
    if request.method == "POST":
        name = request.POST.get("name")
        size = request.POST.get("size")
        if Price_list.objects.filter(name=name, size=size).exists():
            price = Price_list.objects.filter(name=name, size=size)[0].price
            price = (' '.join([str(price)[::-1][i:i + 3] for i in range(0, len(str(price)), 3)]))[::-1]
            date = Price_list.objects.filter(name=name, size=size)[0].date

    all_services = Services.objects.all()[:]
    all_services = transfrom(all_services)
    services_one, services_two, classes = get(all_services)
    calc = {'one' : 'calc__out_open', 'two' : price, 'three' : date}
    # return HttpResponseRedirect("/")
    return render(request, 'index.html', {'all_services': all_services, 'services_one': services_one,
                                          'services_two': services_two, 'classes': classes,
                                          'calc': calc})
# Create your views here.
