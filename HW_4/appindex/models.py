from django.db import models

class Services(models.Model):
    name = models.CharField(max_length=40)
    body = models.CharField(max_length=200)
    price = models.IntegerField()

    def __str__(self):
        return self.name

class Request(models.Model):
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=30)
    select = models.CharField(max_length=20)
    commen = models.CharField(max_length=400)
    def __str__(self):
        return f'{self.name} {self.select}'

class Price_list(models.Model):
    name = models.CharField(max_length=50)
    size = models.CharField(max_length=20)
    price = models.IntegerField()
    date = models.IntegerField()
    def __str__(self):
        return f'{self.name} {self.size}'

# Create your models here.
