from django.urls import path
from .import views

urlpatterns = [
    path('', views.index, name='index'),
    path('send_data/', views.create, name='create'),
    path('calculate/', views.calc, name='calc')
]
