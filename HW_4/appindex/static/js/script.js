function print(text){
    console.log(text);
}

function getParentsContainingClass(elem, desiredСlass, depth = 20) {
    if (elem.classList.contains(desiredСlass)){
        return elem;
    } else {
        return getParentsContainingClass(elem.parentElement, desiredСlass);
    }
}

const nav = document.querySelector('.header__nav');
nav.addEventListener('mouseover', (e) => {
    if (e.target.classList.contains('header__nav__item')){
        e.target.classList.add('header__nav__item_hold');
}})
nav.addEventListener('mouseout', (e) => {
    if (e.target.classList.contains('header__nav__item')){
        e.target.classList.remove('header__nav__item_hold');
}})

const logo = document.querySelector('.header__logo')
logo.addEventListener('mouseover', () => {
    logo.classList.add('header__logo_hold');
})
logo.addEventListener('mouseout', () => {
    logo.classList.remove('header__logo_hold');
})

const portNav = document.querySelector(".portfolio__nav");
const portImg = document.querySelectorAll('.portfolio__img__item');
var portNavItemId = new Array;
for (var i = 0; i < portNav.children.length; i++){
    portNavItemId[i] = "navitem"+(i+1);
}
const portImgSortArr = [[1, 2, 3, 4, 5, 6], [1, 6], [4], [2, 3, 5]];
portNav.addEventListener('click', (event) => {
    if (event.target.classList.contains("portfolio__nav__item")){
        let buttonPortNavBlack = document.querySelector(".portfolio__nav__item.button_black");
        buttonPortNavBlack.classList.remove('button_black');
        buttonPortNavBlack.classList.add('button_white');
        event.target.classList.add('button_black');
        event.target.classList.remove('button_white');

        let portImgSort = portImgSortArr[portNavItemId.indexOf(event.target.id)]
        portImg.forEach((el) =>{
            el.style.display = 'none';
            if (el.classList.contains('portfolio__img__item_full')){
                el.classList.remove('portfolio__img__item_full');
            }
        })
        portImgSort.forEach((el) => {
            portImg[el-1].style.display = 'initial';
            if (!(portNavItemId.indexOf(event.target.id)===0)) {
                portImg[el-1].classList.add('portfolio__img__item_full');
            }
        })
    }
})



const faqItems = document.querySelector('.faq__content');
faqItems.addEventListener('click', (event) => {
    let faqItem = getParentsContainingClass(event.target, 'faq__content__item')
    faqItem.children[0].children[1].classList.toggle('faq__content__item__title__img_clicked')
    faqItem.children[1].classList.toggle('faq__content__item__text_clicked')
})

const calc = document.querySelector('.calc')
const calcOut = document.querySelector('.calc__out');
const calcIn = document.querySelector('.calc__in')
const calcInButton = document.querySelector('.calc__in__button');
const typeWork = document.querySelector('.calc__in__type');
const typeSize = document.querySelector('.calc__in__size');
calcInButton.addEventListener('click', () => {
    if (typeSize.value !== "size_0" && typeWork.value!=="type_0") {
        calcOut.classList.add('calc__out_open')
    } else {
        alert("Обязательно выберите тип и обьем работы для оценивания!")
    }
})

calcInButton.addEventListener('mouseover', () => {
    if (typeSize.value === "size_0" || typeWork.value ==="type_0") {
        calcInButton.classList.add('calc__in__button_off')
        calcInButton.disabled = true;
    }
})

calcIn.addEventListener('mouseover', (event) => {
    if (calcInButton.classList.contains('calc__in__button_off') && event.target.classList.contains('calc__in')) {
        calcInButton.disabled = false;
        calcInButton.classList.remove('calc__in__button_off')

    }
})

if (calcOut.classList.contains('calc__out_open')) {
    calc.scrollIntoView(false);
}

