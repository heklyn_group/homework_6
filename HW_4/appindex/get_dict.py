def get(all_services):
    if len(all_services) % 3 == 0:
        services_one = all_services[:]
        services_two = []
        classes = {'one' : 'services__container1_by3', 'two' : 'display-none'}
    elif len(all_services) % 2 == 0:
        services_one = all_services[:]
        services_two = []
        classes = {'one' : 'services__container1_by2', 'two' : 'display-none'}
    elif len(all_services) % 3 == 1:
        services_one = all_services[0:len(all_services)-1]
        services_two = all_services[len(all_services)-1:]
        classes = {'one' : 'services__container1_by3', 'two' : 'services__container2_by1'}
    else:
        services_one = all_services[0:len(all_services)-2]
        services_two = all_services[len(all_services)-2:]
        classes = {'one': 'services__container1_by3', 'two' : 'services__container2_by2'}
    return services_one, services_two, classes